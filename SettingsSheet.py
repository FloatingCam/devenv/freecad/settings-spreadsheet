'''
*
*    SettingsSheet.py
*
*    Using a simple structure this Macro establishes a spreadsheet
*    of named Properties. Named properties allow you to
*    create designs where a change in one place ripples through the entire design.
*
*    Copyright 2019 Floating Cameras, LLC - Author: Marc Cole
*
*    Licensed under the Apache License, Version 2.0 (the "License");
*    you may not use this file except in compliance with the License.
*    You may obtain a copy of the License at
*
*        http://www.apache.org/licenses/LICENSE-2.0
*
*    Unless required by applicable law or agreed to in writing, software
*    distributed under the License is distributed on an "AS IS" BASIS,
*    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*    See the License for the specific language governing permissions and
*    limitations under the License.
*
'''
_macroName = "SettingsSheet"
_sheetName = "Settings"

import FreeCAD as App
import FreeCADGui as Gui

labelColor = (0.752941, 0.752941, 0.752941, 1.000000)
valueColor = (0.000000, 1.000000, 1.000000, 1.000000)


def processSettings():
    if App.ActiveDocument is None:
        App.newDocument("Unnamed")
    else :
        App.ActiveDocument.recompute()  # Commit any changes (user must have left the cell but this gets the rest)


    pSettings = App.ActiveDocument.getObject(_sheetName)

    # ----------- Create ---------------------------------------
    # When there isn't an existing Settings spreadsheet
    #   Create a Blank Settings spreadsheet
    #   Open the newly created spreadsheet
    if pSettings is None:
        App.ActiveDocument.addObject("Spreadsheet::Sheet", _sheetName)
        Gui.ActiveDocument.setEdit(_sheetName, 0)
        return

    # ------------ Update ---------------------------------------
    # Parse through the cells
    #   Determine Style
    #       -> Name-Value
    #       -> CompoundName-Value
    #   Process Row
    #       -> Set Background color for Labels
    #       -> Set Background color for Named cells
    #       --> Set Aliases

    subNames = []  # Compounded with Name to create CompoundName
    columnCount = 0  # Number of Columns in Compound

    blankCount = 0  # Count of blank lines seen - 3 blank lines terminates the loop

    for currentRow in range(1, 1000):

        currentCellRef = "A{}".format(currentRow)  # Name cell for this row
        valueCellRef = "B{}".format(currentRow)  # First Value cell for this row

        hasName = currentCellRef in pSettings.PropertiesList  # Name cell has a value
        hasValue = valueCellRef in pSettings.PropertiesList  # First value cell has a value

        # Test for blank line
        if hasName is False and \
                hasValue is False:
            blankCount += 1  # Augment the blank line count
            if blankCount > 2:  # Break loop on third blank line
                break  # No more properties

            columnCount = 0  # Blank line always ends a CompoundName set
            continue

        # Test for beginning of a CompoundName set -> Name field is empty
        if hasName is False:
            subNames = []  # Clear any prior subNames
            # Capture column labels for the Compound set.
            for currentCol in range(2, 27):
                subNameRef = "{}{}".format(chr(currentCol + 64), currentRow)
                if subNameRef not in pSettings.PropertiesList:
                    break  # First blank cell terminates the loop

                subNames.append(pSettings.get(subNameRef).replace(" ", ""))  # Save the subname without spaces
                # Set the Label's background color
                pSettings.setBackground("{0}:{0}".format(subNameRef), labelColor)

            columnCount = len(subNames)
            continue

        # --------------- Value Cells Processing --------------------
        # Set Background colors and assign Alias names
        aName = pSettings.get(currentCellRef).replace(" ", "")  # Name label without spaces
        blankCount = 0  # row is not blank so reset blankCount

        # -------------- Name - Value --------------------
        if columnCount is 0:
            if aName not in pSettings.PropertiesList:
                pSettings.setAlias(valueCellRef, aName)  # Set the Value cell alias
            pSettings.setBackground("{0}:{0}".format(currentCellRef), labelColor)
            pSettings.setBackground("{0}:{0}".format(valueCellRef), valueColor)
            continue

        # -------------- Compound Set --------------------
        for cellNo in range(0, len(subNames)):
            valueCellRef = "{}{}".format(chr(66 + cellNo), currentRow)
            cellName = "{}{}".format(aName, subNames[cellNo])

            if cellName not in pSettings.PropertiesList:
                pSettings.setAlias(valueCellRef, cellName)  # Set the Value cell alias
            pSettings.setBackground("{0}:{0}".format(currentCellRef), labelColor)
            pSettings.setBackground("{0}:{0}".format(valueCellRef), valueColor)

        App.ActiveDocument.recompute()
    return pSettings

pSettings = processSettings()

