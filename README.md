# Settings Spreadsheet

Useful for all projects this Macro creates alias values in a spreadsheet based on the labels. 

Using a simple structure this Macro establishes a spreadsheet
of named Properties. Named properties allow you to
create designs where a change in one place ripples through the entire design.

 No code necessary.  Install this Macro and add it to the Menu.  A must for any FreeCAD  designer.
 
 [Complete Instructions - Wiki](https://gitlab.com/FloatingCam/devenv/freecad/settings-spreadsheet/wikis/home)


 **Name Value**

 |   | A | B | C | D |
 | - | - | - | - | - | 
 | 1 |Ring Radius | 10.0  | | |
 | 2 |Ring Width|  2.0 | | |
 | 3 |Output Directory| ~ | | |
 
 **CompoundName Value**
 
 |   | A | B | C | D | E |
 | - | - | - | - | - | - |
 | 1 | | Radius | Width | | |
 | 2 | Ring |  10.0 | 2.0 | | |
 | 3 | | | | | |
 | 4 |Output Directory| ~ | | |