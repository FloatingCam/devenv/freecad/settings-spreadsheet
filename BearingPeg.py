'''
*
*    BearingPeg.py
*
*    The BearingPeg can be highly customized by making changes to
*    values on the Settings spreadsheet.  This demonstrates a
*    highly interactive parametric design in FreeCAD.
*
*    Copyright 2019 Floating Cameras, LLC - Author: Marc Cole
*
*    Licensed under the Apache License, Version 2.0 (the "License");
*    you may not use this file except in compliance with the License.
*    You may obtain a copy of the License at
*
*        http://www.apache.org/licenses/LICENSE-2.0
*
*    Unless required by applicable law or agreed to in writing, software
*    distributed under the License is distributed on an "AS IS" BASIS,
*    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*    See the License for the specific language governing permissions and
*    limitations under the License.
*
*    Changes:
*      Added Changes: Marc Cole - 8/19/2019
*
'''

import FreeCAD as App
import FreeCADGui as Gui

from math import sin,cos,tan,radians,degrees,sqrt

from os.path import expanduser, isfile, isdir
import Part
import Mesh
import Draft, Sketcher

# <editor-fold desc="Globals..." >
# ################################################
# # Globals

workdir = expanduser("~")

part_name = "CustomBearingPeg"
_sheetName = "Settings"

colorValDic = {
    "Black" : (0.0,0.0,0.0),
    "Gray01": (0.376,0.376,0.376),
    "Gray02": (0.533,0.533,0.533),
    "Gray03" : (0.816,0.816,0.816),
    "Gray04": (0.92,0.92,0.92),
    "DarkBlue": (0.000,0.000,1.000),
    "Blue": (0.000,0.667,1.000),
    "LightBlue" : (0.333,1.000,1.000),
    "Red": (0.667, 0.000, 0.000),
    "DarkGreen": (0.000, 0.333, 0.000),
    "Brown": (0.667, 0.333, 0.000),
    "Green": (0.000, 0.667, 0.000),
    "Gray": (0.752941, 0.752941, 0.752941, 1.000000),
    "Aqua": (0.000000, 1.000000, 1.000000, 1.000000),
    "Yellow":  (1.000,1.000,0.000)
}

colorVals = list(colorValDic.values())

labelColor = colorValDic["Gray"]
valueColor = colorValDic["Aqua"]
valueRefColor = colorValDic["Yellow"]

# ################################################
# # End Globals
# </editor-fold desc="Globals..." >

# <editor-fold desc="Support routine and classes...">

def processSettings():
    if App.ActiveDocument is None:
        App.newDocument("Unnamed")
    else :
        App.ActiveDocument.recompute()  # Commit any changes (user must have left the cell but this gets the rest)

    pSettings = App.ActiveDocument.getObject(_sheetName)

    # ----------- Create ---------------------------------------
    # When there isn't an existing Settings spreadsheet
    #   Create a Blank Settings spreadsheet
    #   Open the newly created spreadsheet
    if pSettings is None:
        genSettings()
        pSettings = App.ActiveDocument.getObject(_sheetName)

    # ------------ Update ---------------------------------------
    # Parse through the cells
    #   Determine Style
    #       -> Name-Value
    #       -> CompoundName-Value
    #   Process Row
    #       -> Set Background color for Labels
    #       -> Set Background color for Named cells
    #       --> Set Aliases

    subNames = []  # Compounded with Name to create CompoundName
    columnCount = 0  # Number of Columns in Compound

    blankCount = 0  # Count of blank lines seen - 3 blank lines terminates the loop

    for currentRow in range(1, 1000):

        currentCellRef = "A{}".format(currentRow)  # Name cell for this row
        valueCellRef = "B{}".format(currentRow)  # First Value cell for this row

        hasName = currentCellRef in pSettings.PropertiesList  # Name cell has a value
        hasValue = valueCellRef in pSettings.PropertiesList  # First value cell has a value

        # Test for blank line
        if hasName is False and \
                hasValue is False:
            blankCount += 1  # Augment the blank line count
            if blankCount > 2:  # Break loop on third blank line
                break  # No more properties

            columnCount = 0  # Blank line always ends a CompoundName set
            continue

        # Test for beginning of a CompoundName set -> Name field is empty
        if hasName is False:
            subNames = []  # Clear any prior subNames
            # Capture column labels for the Compound set.
            for currentCol in range(2, 27):
                subNameRef = "{}{}".format(chr(currentCol + 64), currentRow)
                if subNameRef not in pSettings.PropertiesList:
                    break  # First blank cell terminates the loop

                subNames.append(pSettings.get(subNameRef).replace(" ", ""))  # Save the subname without spaces
                # Set the Label's background color
                pSettings.setBackground("{0}:{0}".format(subNameRef), labelColor)

            columnCount = len(subNames)
            continue

        # --------------- Value Cells Processing --------------------
        # Set Background colors and assign Alias names
        aName = pSettings.get(currentCellRef).replace(" ", "")  # Name label without spaces
        blankCount = 0  # row is not blank so reset blankCount

        # -------------- Name - Value --------------------
        if columnCount is 0:
            if aName not in pSettings.PropertiesList:
                pSettings.setAlias(valueCellRef, aName)  # Set the Value cell alias
            pSettings.setBackground("{0}:{0}".format(currentCellRef), labelColor)

            cellVal = pSettings.getContents(valueCellRef)
            # print("RE: {} = cellVal: {}".format(valueCellRef,cellVal))
            if cellVal.startswith("=") :
                pSettings.setBackground("{0}:{0}".format(valueCellRef), valueRefColor)
            else:
                pSettings.setBackground("{0}:{0}".format(valueCellRef), valueColor)
            continue

        # -------------- Compound Set --------------------
        for cellNo in range(0, len(subNames)):
            valueCellRef = "{}{}".format(chr(66 + cellNo), currentRow)
            cellName = "{}{}".format(aName, subNames[cellNo])

            if cellName not in pSettings.PropertiesList:
                pSettings.setAlias(valueCellRef, cellName)  # Set the Value cell alias
            pSettings.setBackground("{0}:{0}".format(currentCellRef), labelColor)
            cellVal = pSettings.getContents(valueCellRef)
            # print("RE: {} = cellVal: {}".format(valueCellRef,cellVal))
            if cellVal.startswith("=") :
                pSettings.setBackground("{0}:{0}".format(valueCellRef), valueRefColor)
            else:
                pSettings.setBackground("{0}:{0}".format(valueCellRef), valueColor)

        App.ActiveDocument.recompute()

        del subNames

    return pSettings

def establishGroup(name, parent_group=None, clean=False):
    if App.ActiveDocument is None:
        cname = part_name
        App.ActiveDocument = App.newDocument(cname)
        # genSettings()
        processSettings()

    group = App.ActiveDocument.getObject(name)
    if group is None:
        group = App.ActiveDocument.addObject("App::DocumentObjectGroup", name)
        App.ActiveDocument.recompute()
        if parent_group :
            parent_group.addObject(group)    # Move group to ingroup
            App.ActiveDocument.recompute()
    else:
        if clean  :
            group.removeObjectsFromDocument()  # Delete the contents

    return group

def shape_extrude(name, shape, height=10.0, symetric=False, reversed=False, taper=0.0, rotate=None, pos=None) :
    ex_ = App.ActiveDocument.addObject('Part::Extrusion', name)
    establishGroup("_Structure_").addObject(ex_)
    ex_.Base = shape
    ex_.DirMode="Custom"

    ex_.Dir = App.Vector(0,0,1)
    ex_.DirLink = None
    ex_.LengthFwd = height
    ex_.LengthRev = 0.0
    ex_.Solid = True
    ex_.Reversed = reversed
    ex_.Symmetric = symetric
    ex_.TaperAngle = taper
    ex_.TaperAngleRev = 0.000000000000000
    if rotate :
        ex_.Placement.Rotation=rotate

    if pos :
        ex_.Placement.Base=App.Vector(pos)

    App.ActiveDocument.recompute()
    return ex_

class Coord:
    def __init__(self,x,y,z=0):
        self.position = [x,y,z]

    @property
    def x (self) :
        return self.position[0]
    @x.setter
    def x (self,val) :
        self.position[0] = val
    @property
    def y (self) :
        return self.position[1]
    @y.setter
    def y (self,val) :
        self.position[1] = val
    @property
    def z (self) :
        return self.position[2]
    @z.setter
    def z (self,val) :
        self.position[2] = val

    @property
    def Vector(self) :
        return App.Vector(self.position[0], self.position[1], self.position[2])

    @property
    def Placement(self):
        return App.Placement(self.Vector, App.Rotation(App.Vector(0,0,1),0))


    def Placement_r(self, pl_rotate) :
        return App.Placement(self.Vector, App.Rotation(pl_rotate[2],pl_rotate[1],pl_rotate[0]))

def difference(name, base, tool, group=None) :
    diff_ = App.ActiveDocument.addObject("Part::Cut", name)
    if group :
        group.addObject(diff_)
    diff_.Base = base
    diff_.Tool = tool

    return diff_

def intersect(name, shapes, pos=None, group=None) :
    intersect_ = App.ActiveDocument.addObject("Part::MultiCommon", name)
    if group :
        group.addObject(intersect_)

    if pos :
        intersect_.Placement.Base=pos.Vector

    intersect_.Shapes = shapes

    return intersect_

def union(name, shapes, pos=None, group=None) :
    union_ = App.ActiveDocument.addObject("Part::MultiFuse", name)
    if group :
        group.addObject(union_)

    if pos :
        union_.Placement.Base=pos.Vector

    union_.Shapes = shapes

    return union_

def mirror(name, source, pos=(0,0,0), rotate=(0,0,0), group=None, normal=(1,0,0)) :
    mirror_ = App.ActiveDocument.addObject("Part::Mirroring", name)
    if group:
        group.addObject(mirror_)
    mirror_.Source = source
    mirror_.Normal = normal
    mirror_.Placement = App.Placement(App.Vector(pos[0], pos[1], pos[2]), App.Rotation(rotate[2], rotate[1], rotate[0]))

    return mirror_

 # </editor-fold desc= "Support routine and classes..." >

# <editor-fold desc="Main..." >
# #######cubetto##################################################
# # Main
def genSettings():
    pSettings = App.ActiveDocument.getObject(_sheetName)
    if pSettings is None:
        pSettings = App.ActiveDocument.addObject("Spreadsheet::Sheet", _sheetName)

        aSettings = [
            ["","BlockDepth","Offset","Retainer","BlockWidth","BlockHeight"],
            ["Wall","6.0","1.0","2.0","=ScrewSeperation + 2 * (ScrewCSRadius + 5)","=AxelRadius + PegBaseHeight + BearingRadius + ScrewOffset"],
            [],
            ["", "Seperation","Radius", "CSRadius", "CSDepth","Offset"],
            ["Screw", "40.0","2.0", "4.5", "2.0","0.0"],
            [],
            ["","Width","BaseWidth","BaseHeight","SpoolWidth","TopHeight"],
            ["Peg","28.0","22.0","6.0","80.0","=AxelRadius+2.0"],
            [],
            ["", "Radius", "Length"],
            ["Axel", "7.0", "122.0"],
            [],
            ["","Radius","Width"],
            ["Bearing","11.2","7.2"],
            []
        ]

        rowNum = 0
        colStart = 65
        for aRow in aSettings:
            rowNum += 1
            if len(aRow) > 0:
                for colNum in range(0, len(aRow)):
                    pSettings.set("{}{}".format(chr(colStart + colNum), rowNum), aRow[colNum])

        del aSettings

        App.ActiveDocument.recompute()

    return

def boxShapeCentered (shapeName, sLength="10.0", sWidth="10.0") :
    activeSketch = App.activeDocument().addObject('Sketcher::SketchObject',shapeName)
    activeSketch.addGeometry(Part.LineSegment(App.Vector(-10.0,-7.5,0.0),App.Vector(10.0, -7.5, 0.0)),False)
    activeSketch.addGeometry(Part.LineSegment(App.Vector(10.0,-7.5,0.0),App.Vector(10.0, 7.5, 0.0)),False)
    activeSketch.addGeometry(Part.LineSegment(App.Vector(10.0,7.5,0.0),App.Vector(-10.0, 7.5, 0.0)),False)
    activeSketch.addGeometry(Part.LineSegment(App.Vector(-10.0,7.5,0.0),App.Vector(-10.0, -7.5, 0.0)),False)
    activeSketch.addConstraint(Sketcher.Constraint("Coincident",0,2,1,1)) # Bottom Right
    activeSketch.renameConstraint(0,"BottomRight")
    activeSketch.addConstraint(Sketcher.Constraint("Coincident",1,2,2,1)) # Upper Right
    activeSketch.renameConstraint(1,"UpperRight")
    activeSketch.addConstraint(Sketcher.Constraint("Coincident",2,2,3,1)) # Upper Left
    activeSketch.renameConstraint(2,"UpperLeft")
    activeSketch.addConstraint(Sketcher.Constraint("Coincident",0,1,3,2)) # Bottom Left
    activeSketch.renameConstraint(3,"LowerLeft")
    activeSketch.addConstraint(Sketcher.Constraint('Symmetric',1,2,0,1,-1,1))
    activeSketch.renameConstraint(4,"Origin")

    activeSketch.addConstraint(Sketcher.Constraint('Distance',2,10.0))
    activeSketch.renameConstraint(5,"Length")
    activeSketch.setExpression('Constraints[5]', sLength)

    activeSketch.addConstraint(Sketcher.Constraint('Distance',3,10.0))
    activeSketch.renameConstraint(6,"Width")
    activeSketch.setExpression('Constraints[6]', sWidth)

    activeSketch.addConstraint(Sketcher.Constraint('Equal',3,1))
    activeSketch.renameConstraint(7,"TopBot")

    activeSketch.addConstraint(Sketcher.Constraint('Equal',0,2))
    activeSketch.renameConstraint(8,"LeftRight")

    activeSketch.addConstraint(Sketcher.Constraint("Vertical",3))
    activeSketch.renameConstraint(9,"SideVertical")
    activeSketch.addConstraint(Sketcher.Constraint("Horizontal",2))
    activeSketch.renameConstraint(10,"SideHorizontal")

    return activeSketch

def setConstraint(dSketch, cSet, cName = None) :
    cI = len(dSketch.Constraints)   # Establish next Id
    dSketch.addConstraint(Sketcher.Constraint(cSet[0], cSet[1],cSet[2],cSet[3],cSet[4],cSet[5]))
    dSketch.setExpression("Constraints[{}]".format(cI), cSet[6])
    if cName :
        dSketch.renameConstraint(cI, cName)

def pegShape (shapeName, subName = "") :

    activeSketch = App.activeDocument().addObject('Sketcher::SketchObject',shapeName)

    aPoints = [
        App.Vector(-1,-1,0),
        App.Vector(1, -1, 0),
        App.Vector(1, 0, 0),
        App.Vector(0, 1, 0),
        App.Vector(-1, 0, 0)
    ]
    # Create the lines
    for sI in range(0,len(aPoints)) :
        sImod = (sI + 1) % len(aPoints)
        activeSketch.addGeometry(Part.LineSegment(aPoints[sI],aPoints[sImod]),False)

    # Establish Properties for Distances

    setConstraint(activeSketch,cSet=("DistanceY",0,1,-1,1,1.0,"Settings.{}BaseHeight".format(subName)),cName="BaseHeight")

    setConstraint(activeSketch, cSet=("DistanceY",-1,1,2,2,1.0,"Settings.{}TopHeight".format(subName)), cName="TopHeight")

    setConstraint(activeSketch, cSet=("DistanceX",3,2,1,2,1.0,"Settings.{}Width".format(subName)), cName="Width")

    setConstraint(activeSketch, cSet=("DistanceX",0,1,0,2,1.0,"Settings.{}BaseWidth".format(subName)),cName="BaseWidth")

    # Connect the lines
    cCnt = len(activeSketch.Constraints)
    for sI in range (0,len(aPoints)) :
        sImod = (sI + 1) % len(aPoints)
        activeSketch.addConstraint(Sketcher.Constraint("Coincident", sI, 2, sImod, 1))
        activeSketch.renameConstraint(cCnt,"Connect{}_{}".format(sI+1, (sI + 2) % (len(aPoints)+1)))
        cCnt += 1

    # Set geometric relationships
    setConstraint(activeSketch,cSet=("Symmetric", 0, 1, 0, 2, -2,"Settings.{}Width".format(subName)),cName="BaseCenter")

    cCnt = len(activeSketch.Constraints)
    activeSketch.addConstraint(Sketcher.Constraint('Symmetric', 1, 2, 3, 2, -1, 1))
    activeSketch.renameConstraint(cCnt, "Origin")
    activeSketch.setExpression("Constraints[{}]".format(cCnt),"Settings.{}Width".format(subName))
    cCnt += 1

    activeSketch.addConstraint(Sketcher.Constraint('PointOnObject',2,2,-2))
    activeSketch.renameConstraint(cCnt, "TopTangent")
    activeSketch.setExpression("Constraints[{}]".format(cCnt),"Settings.{}Width".format(subName))
    cCnt += 1

    activeSketch.addConstraint(Sketcher.Constraint('PointOnObject',3,2,-1))
    activeSketch.renameConstraint(cCnt, "SideTangent")
    activeSketch.setExpression("Constraints[{}]".format(cCnt),"Settings.{}Width".format(subName))
    cCnt += 1

    return activeSketch

def genPeg() :

    dPegShape = pegShape("Peg_Shape","Peg")
    structure_group.addObject(dPegShape)

    pentaCylinder = shape_extrude("PentaCylinder", dPegShape) # The extrusion length is set in setExpression below
    pentaCylinder.setExpression("LengthFwd","Settings.AxelLength + Settings.WallRetainer")
    pentaCylinder.setExpression("Placement.Base.y","-Settings.AxelRadius")
    pentaCylinder.setExpression("Placement.Base.z","Settings.WallOffset")
    del dPegShape

    return pentaCylinder

def genWallBlock() :

    wallBlock = App.ActiveDocument.addObject("Part::Box","WallBlock")
    structure_group.addObject(wallBlock)
    wallBlock.setExpression("Length","Settings.WallBlockWidth")
    wallBlock.setExpression("Width","Settings.WallBlockHeight")
    wallBlock.setExpression("Height","Settings.WallBlockDepth")
    wallBlock.setExpression("Placement.Base.x","-Settings.WallBlockWidth / 2")
    wallBlock.setExpression("Placement.Base.y","-Settings.AxelRadius - Settings.PegBaseHeight")

    WallBlockDepth = pSettings.get("WallBlockDepth")
    __fillets__ = []
    for i in range(len(wallBlock.Shape.Edges)):
        thisEdge = wallBlock.Shape.Edges[i]
        if thisEdge.Length == WallBlockDepth and thisEdge.Curve.Direction == App.Vector(0,0,1):
            __fillets__.append((i+1,5.0,5.0))
    wallBlockDeco=App.ActiveDocument.addObject("Part::Fillet","WallBlockDeco")
    structure_group.addObject(wallBlockDeco)
    wallBlockDeco.Base = wallBlock
    wallBlockDeco.Edges = __fillets__
    del __fillets__
    del wallBlock

    return wallBlockDeco

def genCutter() :
    axel = App.ActiveDocument.addObject("Part::Cylinder","Axel")
    structure_group.addObject(axel)
    axel.setExpression("Radius","Settings.AxelRadius")
    axel.setExpression("Height","Settings.AxelLength")
    axel.setExpression("Placement.Base.z","Settings.WallOffset")

    bearingSlide = App.ActiveDocument.addObject("Part::Box","BearingSlide")
    structure_group.addObject(bearingSlide)
    bearingSlide.setExpression("Length","Settings.BearingRadius * 2", )
    bearingSlide.setExpression("Width","Settings.WallBlockHeight")
    bearingSlide.setExpression("Height","Settings.BearingWidth")
    bearingSlide.setExpression("Placement.Base.x","-Settings.BearingRadius")
    bearingSlide.setExpression("Placement.Base.z","Settings.WallOffset")

    bearing = App.ActiveDocument.addObject("Part::Cylinder", "Bearing")
    structure_group.addObject(bearing)
    bearing.setExpression("Radius", "Settings.BearingRadius")
    bearing.setExpression("Height", "Settings.BearingWidth")
    bearing.setExpression("Placement.Base.z","Settings.WallOffset")

    spoolBearing = App.ActiveDocument.addObject("Part::Cylinder","SpoolBearing")
    structure_group.addObject(spoolBearing)
    spoolBearing.setExpression("Radius","Settings.BearingRadius")
    spoolBearing.setExpression("Height","Settings.BearingWidth")
    spoolBearing.setExpression("Placement.Base.z","Settings.PegSpoolWidth + Settings.WallOffset + Settings.BearingWidth+2*Settings.WallRetainer")

    outsideBearing = App.ActiveDocument.addObject("Part::Cylinder", "OutsideBearing")
    structure_group.addObject(outsideBearing)
    outsideBearing.setExpression("Radius", "Settings.BearingRadius")
    outsideBearing.setExpression("Height", "Settings.BearingWidth")
    outsideBearing.setExpression("Placement.Base.z", "Settings.AxelLength-Settings.BearingWidth+Settings.WallOffset")

    screw = App.ActiveDocument.addObject("Part::Cylinder","Screw")
    structure_group.addObject(screw)
    screw.setExpression("Radius","Settings.ScrewRadius")
    screw.setExpression("Height","Settings.BearingWidth + Settings.WallOffset + Settings.WallRetainer")

    screwCS = App.ActiveDocument.addObject("Part::Cylinder","ScrewCS")
    structure_group.addObject(screwCS)
    screwCS.setExpression("Radius","Settings.ScrewCSRadius")
    screwCS.setExpression("Height","Settings.ScrewCSDepth + 2* Settings.BearingWidth + Settings.WallRetainer")
    screwCS.setExpression("Placement.Base.z","Settings.WallBlockDepth - Settings.ScrewCSDepth")

    screwCutLeft = union("ScrewCutLeft",[screw,screwCS],group=structure_group)
    screwCutLeft.setExpression("Placement.Base.x","-Settings.ScrewSeperation / 2")
    screwCutLeft.setExpression("Placement.Base.y","Settings.WallBlockHeight - Settings.AxelRadius - Settings.PegBaseHeight - Settings.BearingRadius")

    screwCutRight = mirror("ScrewCutRight",screwCutLeft,normal=(1,0,0),group=structure_group)
    del screw
    del screwCS

    pegPinLeft = App.ActiveDocument.addObject("Part::Cylinder","PegPinLeft")
    structure_group.addObject(pegPinLeft)
    pegPinLeft.Radius = 0.20
    # pegPinLeft.Height = 15.0
    pegPinLeft.setExpression("Height","Settings.WallOffset + Settings.BearingWidth + 8")
    pegPinLeft.setExpression("Placement.Base.x","-Settings.PegWidth / 2 + 2")
    pegPinLeft.setExpression("Placement.Base.y","-Settings.AxelRadius")
    pegPinLeft.Placement.Base.z = 2.0

    pegPinRight = mirror("PegPinRight",pegPinLeft,normal=(1,0,0))
    structure_group.addObject(pegPinRight)
    mountTool = union("MountTool",
                 [axel, bearingSlide, bearing, spoolBearing, outsideBearing,screwCutLeft,screwCutRight,pegPinLeft,pegPinRight],
                 group=structure_group)

    del axel
    del bearingSlide
    del bearing
    del spoolBearing
    del outsideBearing
    del screwCutLeft, screwCutRight

    return mountTool

def genPart() :
    if pSettings is None :
        return

    pegCylinder = genPeg()

    wallBlock = genWallBlock()

    bearingPouchShape = boxShapeCentered("BearingPouchShape", sLength="Settings.PegWidth",sWidth="Settings.WallBlockHeight")
    bearingPouch = shape_extrude("BearingPouch",bearingPouchShape,reversed=True)
    bearingPouch.setExpression("LengthFwd","Settings.WallOffset + Settings.WallRetainer + Settings.BearingWidth")
    bearingPouch.setExpression("LengthRev","Settings.WallRetainer")
    bearingPouch.setExpression("Placement.Base.z","Settings.WallOffset + Settings.WallRetainer + Settings.BearingWidth")
    bearingPouch.setExpression("Placement.Base.y","-Settings.AxelRadius - Settings.PegBaseHeight + Settings.WallBlockHeight / 2")
    bearingPouch.TaperAngleRev = -45.0

    mountBase = union("MountBase",
                      [pegCylinder, wallBlock,bearingPouch], # bearingHolder,bearingHolderC],
                      group=structure_group)

    mountTool = genCutter()

    bearingPeg = difference("BearingPeg",mountBase, mountTool,group=part_group)
    bearingPeg.Placement.Rotation = App.Rotation(0,0,90)    # Lay flat on the bed for printing

    del pegCylinder
    del wallBlock
    del bearingPouchShape
    del bearingPouch
    del mountBase
    del mountTool

    return bearingPeg

# ==========================================================================
# ==========================================================================

structure_group = establishGroup("_Structure_",clean=True)
part_group = establishGroup("_Part_",clean=True)
pSettings = App.ActiveDocument.getObject("Settings")
App.ActiveDocument.recompute()
# # End Main
#</editor-fold desc="Main..." >

# ----------------- Insert Logic here -----------------
print("---------------------------------------")
print("Start...")

finishedPart = genPart()

print("Done")

# <editor-fold desc="Cleanup..." >
try:
    Gui.activateWorkbench("DraftWorkbench")
    Gui.ActiveDocument.ActiveView.setAxisCross(True)

    Gui.SendMsgToActiveView("ViewFit")


except Exception:
    None

App.ActiveDocument.recompute()
print("Output")
# Hide all objects
for ShapeNameObj in App.ActiveDocument.Objects:   # hidden alls objects
    if ShapeNameObj :
        _obj = Gui.ActiveDocument.getObject(ShapeNameObj.Name)
        if _obj :
            _obj.Visibility = False

# # Hide all objects in the structure_group
# for part_ in structure_group.Group :
#     gPart = Gui.ActiveDocument.getObject(part_.Label)
#     if gPart:
#         gPart.Visibility = False


# Export the 'Parts' as STL to the working directory
for part_ in part_group.Group :
    if part_ :
        print("Part: {}".format(part_))
        Gui.ActiveDocument.getObject(part_.Label).Visibility = True
        filename = u"{}\\{}_{}.stl".format(workdir, part_name, part_.Label)
        # Mesh.export([part_], filename)
        # print("Exported: {}".format(filename))


    Gui.SendMsgToActiveView("ViewFit")

# </editor-fold desc="Cleanup...">